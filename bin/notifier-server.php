<?php

use Ratchet\Server\IoServer;
use PHPMessenger\Notifier;

error_reporting(E_ALL);
ini_set('display_errors', 1);

require __DIR__.'/../vendor/autoload.php';

$server = IoServer::factory(
    new Notifier(),
    8090
);

$server->run();
