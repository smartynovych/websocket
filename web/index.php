<?php

require __DIR__.'/../vendor/autoload.php';
$config = \Config::getInstance();

if ($config->env == 'dev') {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

include __DIR__.'/../app/view/index.view.php';
