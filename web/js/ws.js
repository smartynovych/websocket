var messageText = '';
var conn = new WebSocket(ws_url);

conn.onopen = function(e) {
    console.log("Connection established!");
};

$(function() {
    var sendMessage = function(objMessage) {
        var currentdate = new Date();
        var datetime = currentdate.getDate() + "."
            + (currentdate.getMonth()+1)  + "."
            + currentdate.getFullYear() + " "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();

        if(objMessage.mtype == 'server') {
            var block = '<div class="alert alert-info"><div class="row">' +
                '<div class="col-md-1"><i class="fa fa-slack fa-2x" aria-hidden="true"></i></div>' +
                '<div class="col-md-3"><i class="fa fa-clock-o" aria-hidden="true"></i> ' + objMessage.mtime + '</div>' +
                '<div class="col-md-8">' + objMessage.textmessage  + '</div>' +
                '</div></div>';
        } else {
            var block = '<div class="alert alert-success"><div class="row">' +
                '<div class="col-md-1"><i class="fa fa-globe fa-2x" aria-hidden="true"></i></div>' +
                '<div class="col-md-3"><i class="fa fa-clock-o" aria-hidden="true"></i> ' + objMessage.mtime + '</div>' +
                '<div class="col-md-8"> ' + objMessage.textmessage  + '</div>' +
                '</div></div>';
        }

        $('div#message-block div').first().before(block);
    };

    conn.onmessage = function(e) {
        var objMessage = $.parseJSON( e.data );
        console.log(objMessage.textmessage);
        sendMessage(objMessage);
    };

    $('#submit').click(function() {
        messageText = $('#message').val();
        if (messageText.length == 0) {
            return false;
        }
        conn.send(messageText);
        return false;
    });
});