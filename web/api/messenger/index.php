<?php

require __DIR__.'/../../../vendor/autoload.php';
$config = \Config::getInstance();

if ($config->env == 'dev') {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

$webSocket = new \PHPMessenger\WebSocket();

if (isset($_POST['token']) AND $_POST['token'] === $config->slack_token) {
    $message = $_POST['text'];
} else {
    $message = 'This is test message for check api';
}

if ($sp = $webSocket->webSocketOpen($config->websocket_host, $config->websocket_port, '', $errstr)) {
    $webSocket->webSocketWrite($sp, $message);
    //echo "Server responded with: " . $webSocket->webSocketRead($sp, $errstr);
}
//echo $errstr;
