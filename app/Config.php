<?php

use Symfony\Component\Yaml\Yaml;

class Config
{
    /**
     * @var Singleton
     */
    private static $instance;
    private $parameters;

    private function __construct()
    {
        $parameters = Yaml::parse(file_get_contents(__DIR__.'/config/parameters.yml'));
        foreach ($parameters as $parameter => $value) {
            $this->{$parameter} = $value;
        }
    }

    public static function getInstance(): Config
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}
