<?php include 'layers/header.view.php'; ?>
    <div class="row">
        <div class="col">
            <div id="message-block">

                <div class="alert alert-info">
                <div class="row">
                    <div class="col-md-1"><i class="fa fa-slack fa-2x" aria-hidden="true"></i></div>
                    <div class="col-md-3"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo date('d.m.Y H:i:s'); ?></div>
                    <div class="col-md-8"> Welcome to our websocket messenger</div>
                </div>
                </div>

            </div>
            <form>
                <div class="form-group">
                    <label for="message">Send Message</label>
                    <textarea class="form-control" id="message" placeholder="Type the message"></textarea>
                </div>
                <button type="submit" class="btn btn-primary" id="submit">Submit</button>
            </form>
        </div>
    </div>
<?php include 'layers/footer.view.php'; ?>