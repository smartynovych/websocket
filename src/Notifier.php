<?php

namespace PHPMessenger;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Notifier implements MessageComponentInterface
{
    protected $clients;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage();
    }

    public function onOpen(ConnectionInterface $conn)
    {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $config = \Config::getInstance();

        $response = [
            //'clientid' => $from->resourceId,
            'mtime' => date('d.m.Y H:i:s'),
            'textmessage' => $msg,
            'mtype' => ($config->server_ip == $from->remoteAddress) ? 'server' : 'client',
        ];

        foreach ($this->clients as $client) {
            //if ($from !== $client) {
            // The sender is not the receiver, send to each client connected
            $client->send(json_encode($response));
            //}
        }

        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" from ip %s to %d other connection%s'."\n", $from->resourceId, $msg, $from->remoteAddress, $numRecv, 1 == $numRecv ? '' : 's');
    }

    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}
