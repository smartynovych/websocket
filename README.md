# Installation

1) git clone git@gitlab.com:smartynovych/websocket.git

2) composer install

3) Rename /app/config/parameters.yml.dist file to /app/config/parameters.yml

   Set current environment parameters:   
   
   env: 'dev' or other
   
   url: 'http://localhost:8000'  - website url
   
   server_ip: '127.0.0.1'  - server external ip
   
   websocket_host: '127.0.0.1'  - websocket listening host
   
   websocket_port: '8080'   - websocket listening port
   
   slack_token: 'xxx'  - Slack token from Outgoing WebHooks configuration
   
4) Run the IO server from command line

   for development environment:
   
     php bin/notifier-web-server.php
     
   or for production 
   
     php -r "exec(\"php /var/www/slackmsg/bin/notifier-web-server.php >>/var/www/slackmsg/logs/script.log 2>&1 &\");"
     
   (for find and kill process use command: ps aux | grep notifier-web-server.php)
   
# Slack Outgoing WebHooks
Create Outgoing WebHooks 

https://api.slack.com/custom-integrations/outgoing-webhooks

Use token for limit messages access to messenger. 